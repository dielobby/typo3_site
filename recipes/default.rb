#
# Cookbook Name:: typo3_site
# Recipe:: default
#
# Copyright Paul Ilea
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

data_bag = data_bag_item('apps', 'global')

base_ip_address = node['typo3_site']['ip_address'] || node['ubuntu_base']['ip_address']
base_hostname = node['typo3_site']['hostname'] || node['ubuntu_base']['hostname']

##########################
### Add hostname entry ###
##########################

hostsfile_entry node['ubuntu_base']['ip_address'] do
	hostname base_hostname
	action :append
end

hostsfile_entry node['ubuntu_base']['ip_address'] do
	hostname node['typo3_site']['server_aliases']
	action :append
end

###################################
### Install additional packages ###
###################################

if node['platform_version'] == '16.04'
	apt_repository('apache2') do
		uri 'http://ppa.launchpad.net/ondrej/apache2/ubuntu'
		deb_src true
		trusted true
		keyserver 'keyserver.ubuntu.com'
		key 'E5267A6C'
		distribution 'xenial'
		components [:main]
		arch 'amd64'
		action :add
	end

	apt_repository('php7.1') do
		uri 'http://ppa.launchpad.net/ondrej/php/ubuntu'
		deb_src true
		trusted true
		keyserver 'keyserver.ubuntu.com'
		key 'E5267A6C'
		distribution 'xenial'
		components [:main]
		arch 'amd64'
		action :add
	end

	execute 'apt -y update'
end

if node['typo3_site']['use_graphicsmagick']
	include_recipe 'graphicsmagick'
else
	include_recipe 'imagemagick'
end

include_recipe 'apache2'
include_recipe 'apache2::mod_ssl'

package 'sshpass' do
	action :install
end

if node['platform_version'] == '16.04'
	package 'libapache2-mod-php' + node['ubuntu_base']['php_version']
	if node['ubuntu_base']['php_version'] == '7.0'
		%w(php7.0 php7.0-cli php7.0-curl php7.0-intl php7.0-gd php7.0-mcrypt php7.0-mysql php7.0-xml php7.0-json php7.0-mbstring php7.0-soap php7.0-zip php7.0-imagick).each do |name|
			package name do
				action :install
			end
		end
	else
		if node['ubuntu_base']['php_version'] == '7.1'
			%w(php7.1 php7.1-cli php7.1-curl php7.1-intl php7.1-gd php7.1-mcrypt php7.1-mysql php7.1-xml php7.1-json php7.1-mbstring php7.1-soap php7.1-zip php7.1-imagick).each do |name|
				package name do
					action :install
				end
			end
		else
			if node['ubuntu_base']['php_version'] == '7.2'
				%w(php7.2 php7.2-cli php7.2-curl php7.2-intl php7.2-gd php7.2-mysql php7.2-xml php7.2-json php7.2-mbstring php7.2-soap php7.2-zip php7.2-imagick).each do |name|
					package name do
						action :install
					end
				end
			else
				if node['ubuntu_base']['php_version'] == '7.3'
					%w(php7.3 php7.3-cli php7.3-curl php7.3-intl php7.3-gd php7.3-mysql php7.3-xml php7.3-json php7.3-mbstring php7.3-soap php7.3-zip php7.3-imagick).each do |name|
						package name do
							action :install
						end
					end
				else
					# no mcrypt anymore
					%w(php php-cli php-curl php-intl php-gd php-mysql php-xml php-json php-mbstring php-soap php-zip php-imagick).each do |name|
						package name do
							action :install
						end
					end
				end
			end
		end
	end

	apache_module 'http2' do
		enable node['typo3_site']['allow_http2']
	end
else
	include_recipe 'apache2::mod_php5'
	%w(php5-curl php5-intl php5-gd php5-mcrypt php5-mysql php5-imagick).each do |name|
		package name do
			action :install
		end
	end
end

%w{expires headers ssl deflate rewrite}.each do |name|
	apache_module name do
		enable true
	end
end

################################
### Modify PHP Configuration ###
################################

if node['platform_version'] == '16.04'
	replace_or_add 'Increase time limit' do
		path '/etc/php/' + node['ubuntu_base']['php_version'] + '/apache2/php.ini'
		pattern 'max_execution_time =.*'
		line 'max_execution_time = 240'
	end

	replace_or_add 'Increase time limit - CLI' do
		path '/etc/php/' + node['ubuntu_base']['php_version'] + '/cli/php.ini'
		pattern 'max_execution_time =.*'
		line 'max_execution_time = 240'
	end

	replace_or_add 'Increase memory limit' do
		path '/etc/php/' + node['ubuntu_base']['php_version'] + '/apache2/php.ini'
		pattern 'memory_limit =.*'
		line 'memory_limit = 512M'
	end

	replace_or_add 'Increase upload size limit' do
		path '/etc/php/' + node['ubuntu_base']['php_version'] + '/apache2/php.ini'
		pattern 'upload_max_filesize =.*'
		line 'upload_max_filesize = 64M'
	end

	replace_or_add 'Increase upload size limit - CLI' do
		path '/etc/php/' + node['ubuntu_base']['php_version'] + '/cli/php.ini'
		pattern 'upload_max_filesize =.*'
		line 'upload_max_filesize = 64M'
	end

	replace_or_add 'Increase post_max_size limit' do
		path '/etc/php/' + node['ubuntu_base']['php_version'] + '/apache2/php.ini'
		pattern 'post_max_size =.*'
		line 'post_max_size = 256M'
	end

	replace_or_add 'Increase post_max_size limit - CLI' do
		path '/etc/php/' + node['ubuntu_base']['php_version'] + '/cli/php.ini'
		pattern 'post_max_size =.*'
		line 'post_max_size = 256M'
	end

	replace_or_add 'Set date.timezone' do
		path '/etc/php/' + node['ubuntu_base']['php_version'] + '/apache2/php.ini'
		pattern 'date.timezone =.*'
		line 'date.timezone = Europe/Berlin'
	end

	replace_or_add 'Set date.timezone - CLI' do
		path '/etc/php/' + node['ubuntu_base']['php_version'] + '/cli/php.ini'
		pattern 'date.timezone =.*'
		line 'date.timezone = Europe/Berlin'
	end

	replace_or_add 'Set max_input_vars' do
		path '/etc/php/' + node['ubuntu_base']['php_version'] + '/apache2/php.ini'
		pattern '.*max_input_vars =.*'
		line 'max_input_vars = 1500'
	end

	replace_or_add 'Set max_input_vars - CLI' do
		path '/etc/php/' + node['ubuntu_base']['php_version'] + '/cli/php.ini'
		pattern '.*max_input_vars =.*'
		line 'max_input_vars = 1500'
	end

	replace_or_add 'Set curl ca certificate' do
		path '/etc/php/' + node['ubuntu_base']['php_version'] + '/apache2/php.ini'
		pattern '.*curl.cainfo =.*'
		line 'curl.cainfo = /etc/apache2/ssl/rootCA.pem'
	end

	replace_or_add 'Set curl ca certificate - CLI' do
		path '/etc/php/' + node['ubuntu_base']['php_version'] + '/cli/php.ini'
		pattern '.*curl.cainfo =.*'
		line 'curl.cainfo = /etc/apache2/ssl/rootCA.pem'
	end
else
	replace_or_add 'Increase time limit' do
		path '/etc/php5/apache2/php.ini'
		pattern 'max_execution_time =.*'
		line 'max_execution_time = 240'
	end

	replace_or_add 'Increase time limit - CLI' do
		path '/etc/php5/cli/php.ini'
		pattern 'max_execution_time =.*'
		line 'max_execution_time = 240'
	end

	replace_or_add 'Increase memory limit' do
		path '/etc/php5/apache2/php.ini'
		pattern 'memory_limit =.*'
		line 'memory_limit = 512M'
	end

	replace_or_add 'Increase upload size limit' do
		path '/etc/php5/apache2/php.ini'
		pattern 'upload_max_filesize =.*'
		line 'upload_max_filesize = 64M'
	end

	replace_or_add 'Increase upload size limit - CLI' do
		path '/etc/php5/cli/php.ini'
		pattern 'upload_max_filesize =.*'
		line 'upload_max_filesize = 64M'
	end

	replace_or_add 'Increase post_max_size limit' do
		path '/etc/php5/apache2/php.ini'
		pattern 'post_max_size =.*'
		line 'post_max_size = 256M'
	end

	replace_or_add 'Increase post_max_size limit - CLI' do
		path '/etc/php5/cli/php.ini'
		pattern 'post_max_size =.*'
		line 'post_max_size = 256M'
	end

	replace_or_add 'Set date.timezone' do
		path '/etc/php5/apache2/php.ini'
		pattern 'date.timezone =.*'
		line 'date.timezone = Europe/Berlin'
	end

	replace_or_add 'date.timezone - CLI' do
		path '/etc/php5/cli/php.ini'
		pattern 'date.timezone =.*'
		line 'date.timezone = Europe/Berlin'
	end
end

################################
### Setup Apache Environment ###
################################

# create ssl certificate
# Instructions are from here: https://stackoverflow.com/questions/7580508/getting-chrome-to-accept-self-signed-localhost-certificate/43666288#43666288
template '/etc/apache2/ssl/v3.ext' do
	source 'v3.ext'
end

template '/etc/apache2/ssl/create_certificate_for_domain.sh' do
	source 'create_certificate_for_domain.sh'
end

# not needed (globally used, but maybe someone else needs this)
# template 'create_root_cert_and_key.sh' do
# 	source 'create_root_cert_and_key.sh'
# end

template '/etc/apache2/ssl/rootCA.key' do
	source 'rootCA.key'
end

template '/etc/apache2/ssl/rootCA.pem' do
	source 'rootCA.pem'
end

# Serial file (don't use that as you can't create more than one certificate if you add this!)
# template '/etc/apache2/ssl/rootCA.srl' do
# 	source 'rootCA.srl'
# end

# concatenate the additional optional domains in order to avoid missing index exceptions
additional_domains = ''
if node.include?('xhprof') && node['xhprof'].include?('hostname') && node['xhprof']['hostname']
	additional_domains = additional_domains + ' ' + node['xhprof']['hostname']
else
	additional_domains = additional_domains + ' xhprof.' + base_hostname
end

if node.include?('webgrind') && node['webgrind'].include?('hostname') && node['webgrind']['hostname']
	additional_domains = additional_domains + ' ' + node['webgrind']['hostname']
else
	additional_domains = additional_domains + ' webgrind.' + base_hostname
end

if node.include?('mailhog') && node['mailhog'].include?('hostname') && node['mailhog']['hostname']
	additional_domains = additional_domains + ' ' + node['mailhog']['hostname']
else
	additional_domains = additional_domains + ' mailhog.' + base_hostname
end

bash 'Create Certificate' do
	cwd '/etc/apache2/ssl/'
	code <<-EOF
		chmod 755 create_certificate_for_domain.sh
		./create_certificate_for_domain.sh #{base_hostname} #{node['typo3_site']['server_aliases']}#{additional_domains}
	EOF
	action :run
end

# create vHost directory
directory "#{node['typo3_site']['webroot']}/#{base_hostname}" do
	owner 'vagrant'
	group data_bag['groupId']
	mode '0755'
	action :create
end

# create web app with an own virtual host
if node['platform_version'] == '16.04'
	web_app base_hostname do
		template 'vHost-xenial.conf.erb'
		docroot "#{node['typo3_site']['webroot']}/#{base_hostname}/#{node['typo3_site']['web_directory']}/"
		server_name base_hostname
		server_aliases node['typo3_site']['server_aliases']
	end
else
	web_app base_hostname do
		template 'vHost.conf.erb'
		docroot "#{node['typo3_site']['webroot']}/#{base_hostname}/#{node['typo3_site']['web_directory']}/"
		server_name base_hostname
		server_aliases node['typo3_site']['server_aliases']
		server_aliases node['typo3_site']['server_aliases']
	end
end

# The apache must be running on the main group of the user that can be different, because of the group mapping feature
replace_or_add 'Change the apache group to the users main group' do
	path '/etc/apache2/envvars'
	pattern 'export APACHE_RUN_GROUP=.*'
	line "export APACHE_RUN_GROUP=#{data_bag['groupId']}"
end

service 'apache2' do
	action :restart
end

#######################
### Provide Sources ###
#######################

file '/home/vagrant/ssh_wrapper.sh' do
	owner 'vagrant'
	group data_bag['groupId']
	mode '0755'
	content "#!/bin/sh\nexec /usr/bin/ssh -o \"StrictHostKeyChecking=no\" \"$@\""
end

if node['typo3_site']['repository'] != ''
	ssh_known_hosts_entry node['typo3_site']['repository_hostname'] do
		port node['typo3_site']['repository_port']
	end

	git "#{node['typo3_site']['webroot']}/#{base_hostname}" do
		repository node['typo3_site']['repository']
		remote 'origin'
		revision node['typo3_site']['repository_branch']
		timeout 100000
		user 'vagrant'
		group data_bag['groupId']
		action :checkout
		ssh_wrapper '/home/vagrant/ssh_wrapper.sh'
	end
end

###################
### Setup TYPO3 ###
###################

bash 'Setup TYPO3' do
	cwd '/home/vagrant/'

	user 'vagrant'
	group data_bag['groupId']
	timeout 3000000
	only_if {node['typo3_site']['download_typo3']}

	code <<-EOF
		if [ ! -d #{node['typo3_site']['webroot']}/typo3_src-#{node['typo3_site']['typo3_version']} ]
		then
			wget http://get.typo3.org/#{node['typo3_site']['typo3_version']} \
				-O typo3_src-#{node['typo3_site']['typo3_version']}.tgz

			mkdir -p #{node['typo3_site']['webroot']}/typo3_src-#{node['typo3_site']['typo3_version']}
			tar -xzf typo3_src-#{node['typo3_site']['typo3_version']}.tgz --strip-components=1 \
				-C #{node['typo3_site']['webroot']}/typo3_src-#{node['typo3_site']['typo3_version']};
		fi

		mkdir -p #{node['typo3_site']['webroot']}/#{base_hostname}/#{node['typo3_site']['web_directory']}
		cd #{node['typo3_site']['webroot']}/#{base_hostname}/#{node['typo3_site']['web_directory']}
		rm -f typo3_src
		ln -s ../typo3_src-#{node['typo3_site']['typo3_version']} typo3_src
		touch typo3conf/ENABLE_INSTALL_TOOL

		if [ ! -e typo3 ]
		then
			ln -s typo3_src/typo3 typo3
		fi

		if [ ! -e index.php ]
		then
			ln -s typo3_src/index.php index.php
		fi
	EOF
	action :run
end

#########################
### Synchronize Files ###
#########################

node['typo3_site']['sync_directories'].each do |sync_data|
	# don't fix this. This must look exactly like this! Foodcritic is not correct here!
	if sync_data['hostname']
		ssh_known_hosts_entry sync_data['hostname'] do
			port sync_data['port']
		end
	end

	bash "Synchronize #{sync_data['local']}" do
		user 'vagrant'
		group data_bag['groupId']
		timeout 3000000
		only_if {
			sync_data['sync'] or not ::File.exists?("#{node['typo3_site']['webroot']}/#{base_hostname}/#{node['typo3_site']['web_directory']}/#{sync_data['local']}")
		}

		options = '--delete -auvz'
		if sync_data['options']
			options = sync_data['options']
		end

		additional_options = ''
		if sync_data['additional_options']
			additional_options = sync_data['additional_options']
		end

		if sync_data['hostname']
			sync_password = ''
			if sync_data['password']
				sync_password = sync_data['password']
				if sync_password != ''
					sync_password = "sshpass -p #{sync_password}"
				end
			end

			code <<-EOF
				rsync #{options} #{additional_options} --progress -e "#{sync_password} ssh -p #{sync_data['port']} -o StrictHostKeyChecking=no" \
					--no-o --no-g #{sync_data['user']}@#{sync_data['hostname']}:#{sync_data['remote']} \
					"#{node['typo3_site']['webroot']}/#{base_hostname}/#{node['typo3_site']['web_directory']}/#{sync_data['local']}";
			EOF
		else
			code <<-EOF
				rsync #{options} #{additional_options} --progress --no-o --no-g #{sync_data['remote']} \
				"#{node['typo3_site']['webroot']}/#{base_hostname}/#{node['typo3_site']['web_directory']}/#{sync_data['local']}";
			EOF
		end
		action :run
	end
end

####################
### Create Links ###
####################

node['typo3_site']['create_links'].each do |link_data|
	execute 'remove link target' do
		command "rm -f #{link_data['target']}"
		action :run
	end

	link link_data['source'] do
		to link_data['target']
	end
end

###########################################
### Provide site specific configuration ###
###########################################

directory "#{node['typo3_site']['webroot']}/#{base_hostname}/#{node['typo3_site']['web_directory']}/typo3conf/" do
	owner 'vagrant'
	mode '0755'
	action :create
end

template "#{node['typo3_site']['webroot']}/#{base_hostname}/#{node['typo3_site']['web_directory']}/typo3conf/#{node['typo3_site']['site_configuration']}" do
	source 'SiteConfiguration.php.erb'
	owner 'vagrant'
	mode '0660'
	variables ({
		:group_id => data_bag['groupId'],
		:additional_configuration => node['typo3_site']['additional_configuration']
	})
end

template "#{node['typo3_site']['webroot']}/#{base_hostname}/#{node['typo3_site']['web_directory']}/typo3conf/AdditionalConfiguration.php" do
	source 'AdditionalConfiguration.php'
	owner 'vagrant'
	mode '0660'
	action :create_if_missing
end

###################
### Setup MySQL ###
###################

mysql_service 'default' do
	version node['typo3_site']['mysql_version']
	initial_root_password 'root'
	action [:create, :start]
end

mysql_config 'default' do
	source "my-#{node['typo3_site']['mysql_version']}.cnf.erb"
	action :create
end

mysql_client 'default' do
	version node['typo3_site']['mysql_version']
	action :create
end

# important, because the set configuration isn't used otherwise in the next steps
mysql_service 'default' do
	action :restart
end

########################
### Create Databases ###
########################

# install the necessary mysql2 gem
mysql2_chef_gem 'default' do
	action :install
end

# create the databases
connection_info = {:host => '127.0.0.1', :username => 'root', :password => 'root'}

node['typo3_site']['sync_databases'].each do |database_data|
	# create database
	mysql_database database_data['database_name'] do
		connection connection_info
		action :create
	end

	# create user
	mysql_database_user database_data['database_user'] do
		connection connection_info
		database_name database_data['database_name']
		password database_data['database_password']
		host '127.0.0.1'
		privileges [:all]
		action :grant
	end

	if database_data['dump_hostname']
		# add host to ssh known hosts
		ssh_known_hosts_entry database_data['dump_hostname'] do
			port database_data['dump_port']
		end

		# download the sql data
		bash 'Download Dump for ' + database_data['database_name'] do
			user 'vagrant'
			group data_bag['groupId']
			timeout 3000000

			dump_password = ''
			if database_data['dump_password']
				dump_password = database_data['dump_password']
				if dump_password != ''
					dump_password = "sshpass -p #{dump_password}"
				end
			end

			code <<-EOF
				rsync --delete -auvz --progress -e "#{dump_password} ssh -p #{database_data['dump_port']} -o StrictHostKeyChecking=no" \
					--no-o --no-g #{database_data['dump_user']}@#{database_data['dump_hostname']}:#{database_data['dump_remote']} \
					#{database_data['dump_local']};
			EOF
			action :run
		end
	end

	if database_data['dump_local'] and database_data['database_name']
		# load the dump
		bash 'Import Dump for ' + database_data['database_name'] do
			code <<-EOF
				mysql -h 127.0.0.1 -u root -proot #{database_data['database_name']} < #{database_data['dump_local']}
			EOF
			timeout 10000
			action :run
		end

		# causes still strange issues that leads to crashes of the MySQL server
		# don't waste more time on this one as it seems like he is executing this as one
		# really big sql query and then runs into heavy limitation issues. The way above is much more stable.
		# mysql_database 'Import Dump for ' + database_data['database_name'] do
		# 	connection connection_info
		# 	database_name database_data['database_name']
		# 	sql { ::File.open(database_data['dump_local']).read }
		# 	action :query
		# end

		if database_data['post_install_queries']
			database_data['post_install_queries'].each do |query|
				mysql_database 'Execute Post Install Query' do
					connection connection_info
					database_name database_data['database_name']
					sql query
					action :query
				end
			end
		end
	end

	if database_data['dump_hostname'] and database_data['dump_local']
		bash 'Remove downloaded ' + database_data['dump_local'] do
			code <<-EOF
				rm -f #{database_data['dump_local']}
			EOF
			action :run
		end
	end
end

##################
### Setup Solr ###
##################

if node['typo3_site']['solr']['init']
	include_recipe 'java'
	include_recipe 'typo3_solr'

	typo3_solr_app 'solr' do
		solr node['typo3_site']['solr']['version_solr']
		extension node['typo3_site']['solr']['version_extension']
		tika_server node['typo3_site']['solr']['version_tika_server']
	end

	node['typo3_site']['solr']['cores'].each do |core_data|
		typo3_solr_core core_data['name'] do
			language core_data['language']
			config_directory node['typo3_site']['solr']['config_directory']
			action :add
			ignore_failure true
		end
	end

	execute 'solr-updateConnections' do
		command "php #{node['typo3_site']['webroot']}/#{base_hostname}/#{node['typo3_site']['web_directory']}/typo3/cli_dispatch.phpsh extbase solr:updateconnections || true"
		user 'vagrant'
		group data_bag['groupId']
		action :run
	end
end
