default['typo3_site']['ip_address'] = false
default['typo3_site']['hostname'] = false
default['typo3_site']['webroot'] = '/var/www'
default['typo3_site']['site_configuration'] = 'SiteConfiguration.php'
default['typo3_site']['additional_configuration'] = ''
default['typo3_site']['server_aliases'] = 'www.site.dev'
default['typo3_site']['web_directory'] = ''
default['typo3_site']['allow_http2'] = false
default['typo3_site']['repository'] = ''
default['typo3_site']['repository_branch'] = 'master'
default['typo3_site']['repository_hostname'] = ''
default['typo3_site']['repository_port'] = 22
default['typo3_site']['download_typo3'] = false
default['typo3_site']['typo3_version'] = '7.6'
default['typo3_site']['use_graphicsmagick'] = false

if node['platform_version'] == '16.04'
	default['typo3_site']['mysql_version'] = '5.7'
else
	default['typo3_site']['mysql_version'] = '5.6'
end

# Default Solr Configuration
default['typo3_site']['solr']['init'] = false
default['typo3_site']['solr']['version_solr'] = '7.6.0'
default['typo3_site']['solr']['version_extension'] = '9.0.0'
default['typo3_site']['solr']['version_tika_server'] = '1.23'
default['typo3_site']['solr']['config_directory'] = 'ext_solr_9_0_0'
default['typo3_site']['solr']['cores'] = [
	{
		:name => 'core-en_US',
		:language => 'english'
	},
	{
		:name => 'core-de_DE',
		:language => 'german'
	},
]

# Example:
#[
#	{
#		:user => 'vagrant',
#		:hostname => 'deploy.site.com',
#		:port => 22,
#		:remote => '~/site/fileadmin/',
#		:local => 'fileadmin',
#		:sync => true
#	},
#	{
#		:user => 'vagrant',
#		:hostname => 'deploy.site.com',
#		:port => 22,
#		:remote => '~/site/uploads/',
#		:local => 'uploads',
#		:sync => true
#	}
#]
default['typo3_site']['sync_directories'] = []

# Example:
#[
#	{
#		:database_name => 'typo3',
#		:database_user => 'typo3',
#		:database_password => 'typo3',
#		:dump_user => 'vagrant',
#		:dump_hostname => 'deploy.site.com',
#		:dump_port => 22,
#		:dump_remote => '~/site/dump.sql',
#		:dump_local => '/home/vagrant/dump.sql',
#		:post_install_queries => [
#			"INSERT INTO typo3.be_users (pid, tstamp, username, password, admin, usergroup, disable, starttime, endtime, lang, email) VALUES (0,1276860841,'admin','$1$lV7klIfP$E8.y9jiK4RL6qZzgWboSP/',1,'0',0,0,0,'','admin@example.com')",
#			"UPDATE typo3.sys_domain SET domainName = 'site.dev' WHERE uid = 1;"
#		]
#	}
#]
default['typo3_site']['sync_databases'] = []

# Example:
# [
#   {
#       :source => '/var/www/mySite/fileadmin',
#       :target => '/var/www/fileadmin',
#   }
# ]
default['typo3_site']['create_links'] = []
